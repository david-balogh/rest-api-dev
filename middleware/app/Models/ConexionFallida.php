<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConexionFallida
{
    public $error;
    public $error_description;

    function __construct() {
        $this->error = true;
        $this->error_description = 'unavailable_connection';
    }

    public function toJson($options = 0){
        return json_encode($this);
    }
}