<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Cache;
use Closure;
use Illuminate\Http\Response;

class MiddlewareApiBefore
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authorization = $request->header('Authorization');
        
        $validation = Cache::get( $authorization );
        if( isset($validation) ){
            $response = $next($request);
        }
        else{
            $status = 401;
            $conexionNoAutorizada = new \App\Models\ConexionNoAutorizada();
            $content = $conexionNoAutorizada->toJson();

            $response = (new Response($content, $status))->header('Content-Type', 'application/json');
        }
        
        return $response;
    }

    public function terminate($request, $response)
    {

        
    }
}
