<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Cache;
use Closure;

class MiddlewareApiLoginAfter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        // Perform action
        
        $responseParsed = json_decode($response->getContent());
        if(isset($responseParsed) && $responseParsed->error == false){
            Cache::put($responseParsed->access_token, $responseParsed->person_id, env('CACHE_TIME'));
        }
        
        return $response;
    }

    public function terminate($request, $response)
    {
        // Store the session data...
        
        
    }
}
