<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Facades\Cache;

class APIHandler extends BaseController
{
    public function Login(Request $request)
    {
        return $this->CallInternalAPI($request, 'POST', env('MS_LOGIN_URL'));
    }

    public function Maquinas(Request $request)
    {
        $authorization = $request->header('Authorization');
        $person_id = Cache::get( $authorization );

        return $this->CallInternalAPI($request, 'GET', env('MS_CORE_URL')."/maquinas/".strval($person_id));
    }

    public function Dispositivos(Request $request)
    {
        $authorization = $request->header('Authorization');
        $person_id = Cache::get( $authorization );

        return $this->CallInternalAPI($request, 'GET', env('MS_CORE_URL') ."/dispositivos/". strval($person_id) ."/". $request->device_id);
    }

    public function Persona(Request $request)
    {
        $authorization = $request->header('Authorization');
        $person_id = Cache::get( $authorization );

        return $this->CallInternalAPI($request, 'GET', env('MS_CORE_URL')."/persona/".strval($person_id));
    }


    private function CallInternalAPI(Request $request, $method, $endpoint)
    {
        $client = new \GuzzleHttp\Client();

        try{
            $response = $client->request($method, $endpoint, ['body' => $request->getContent()]);
            $status = $response->getStatusCode();
            $content = $response->getBody();
        }
        catch(RequestException $e){
            $status = $e->getResponse()->getStatusCode();
            $content = $e->getResponse()->getBody();
        }
        catch(ConnectException $e){
            $status = 500;
            $conexionFallida = new \App\Models\ConexionFallida();
            $content = $conexionFallida->toJson();
        }
        
        
        return (new Response($content, $status))
                  ->header('Content-Type', 'application/json');
    }
}
