<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

# Commands:
# php artisan make:seeder PersonSeeder
# php artisan db:seed
# Str::random(10).

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app('db')->table('person')->insert([
            'dni' => '31497766',
            'nombre' => 'David',
            'apellido' => 'Balogh',
            'direccion' => 'Vidal 2261. CABA.',
            'username' => 'dave',
            'password' => md5('123'),
            'legajo' => '63384',
        ]);
    }
}
