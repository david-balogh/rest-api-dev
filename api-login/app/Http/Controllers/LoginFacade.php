<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;
use \Illuminate\Support\Facades\Hash;

class LoginFacade extends BaseController
{
    protected $username;
    protected $password;

    public function autenticar(Request $request)
    {
        //validar content-type
        $request = json_decode($request->getContent());

        $person = new \App\Models\Administrador($request->username);
        
        if($person->getId() != null && $person->getPassword() == md5($request->password)){
            $loginResultado = new \App\Models\LoginExitoso();
            $loginResultado->person_id = $person->getId();
            $loginResultado->person_name = $person->getNombre()." ".$person->getApellido();
        }
        else{
            $loginResultado = new \App\Models\LoginFallido();
        }
        
        
        $content = $loginResultado->toJson();
        $status = $loginResultado->getStatusCode();
        
        return (new Response($content, $status))
                  ->header('Content-Type', 'application/json');
    }
}