<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConexionNoAutorizada
{
    public $error;
    public $error_description;

    function __construct() {
        $this->error = true;
        $this->error_description = 'unauthorized_connection';
    }

    public function toJson($options = 0){
        return json_encode($this);
    }
}