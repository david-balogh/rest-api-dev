<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class LoginExitoso
{
    public $error;
    public $access_token;
    public $token_type;
    public $issued_at;
    public $person_id;
    public $person_name;

    function __construct() {
        $this->error = false;
        $this->access_token = strtoupper(md5(uniqid()));
        $this->token_type = 'Bearer';
        $this->issued_at = microtime(true);
    }

    public function toJson($options = 0){
        return json_encode($this);
    }

    public function getStatusCode(){
        return 200;
    }
}