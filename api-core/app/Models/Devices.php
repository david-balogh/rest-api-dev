<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Devices extends \App\Models\BaseResponse
{
	public $machine_id;
	public $machine_name;

	function cargarContenido(){

		
		
		$query = "SELECT * FROM device where person_id=".strval($this->person_id);

		if($this->machine_id != 0){
			$query = $query." and machine_id=".strval($this->machine_id);

			$machine = app('db')->select("SELECT * FROM machine where person_id=".strval($this->person_id)." and id=".strval($this->machine_id)." limit 1");
			if(sizeof($machine) == 1){
				$this->machine_name = $machine[0]->machine_name;
			}
		}
		else{
			$this->machine_name = "Todos Mis Dispositivos";
		}

		$devices = app('db')->select($query);
		
		foreach($devices as $device_db){
			$device = new \App\Models\Device();
			$device->id = $device_db->id;
			$device->machine_id = $device_db->machine_id;
			$device->person_id = $device_db->person_id;
			$device->device_name = $device_db->device_name;
			$device->device_type = $device_db->device_type;
			$device->device_information = $device_db->device_information;
			$device->device_configuration = $device_db->device_configuration;

			array_push( $this->content, $device );
		}
	}
 
	public function setMachineId($machine_id)
	{
		$this->machine_id = intval($machine_id);

		return $this;
	}
}