<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Machine
{
    public $id;
    public $person_id;
	public $machine_name;
 
    function __construct($person_id) {
		$this->person_id = $person_id;
	}

	public function toJson($options = 0){
        return json_encode($this);
    }
}