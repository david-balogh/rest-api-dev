<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Device
{
	public $id;
    public $machine_id;
    public $person_id;
	public $device_name;
	public $device_type;
	public $device_information;
	public $device_configuration;

	public function toJson($options = 0){
        return json_encode($this);
    }
}