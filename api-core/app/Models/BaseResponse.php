<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


abstract class BaseResponse
{
	public $error = false;
	public $error_description;
    public $person_id;
	public $content = array();
 
    public function __construct($person_id) {
		$this->person_id = intval($person_id);
	}

	abstract function cargarContenido();

	public function toJson($options = 0){
        return json_encode($this);
    }
}