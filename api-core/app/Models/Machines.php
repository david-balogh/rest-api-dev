<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Machines extends \App\Models\BaseResponse
{
	function cargarContenido(){
		$machines = app('db')->select("SELECT * FROM machine where person_id=".$this->person_id);
		
		foreach($machines as $machine_db){
			$machine = new \App\Models\Machine(intval($this->person_id));
			$machine->id = $machine_db->id;
			$machine->machine_name = $machine_db->machine_name;

			array_push( $this->content, $machine );
		}
	}
}