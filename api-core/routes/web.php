<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//$router->post('/core', 'CoreFacade@agregar');

$router->get('/core/maquinas/{person_id}', 'CoreFacade@maquinas');

$router->get('/core/dispositivos/{person_id}/{machine_id}', 'CoreFacade@dispositivos');

$router->get('/core/persona/{person_id}', 'CoreFacade@persona');